require('./bootstrap');

window.Vue = require('vue');
window.Event = new Vue();

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

Vue.component(
	'admin-sidebar',
	require('./components/admin-sidebar.vue').default
);

Vue.component(
	'admin-users',
	require('./components/contents/admin-users.vue').default
);
Vue.component(
    'admin-users-insert',
    require('./components/inserts/admin-users.vue').default
);

Vue.component(
    'admin-movies',
    require('./components/contents/admin-movies.vue').default
);
Vue.component(
    'admin-movies-insert',
    require('./components/inserts/admin-movies.vue').default
);

Vue.component(
    'admin-shows',
    require('./components/contents/admin-shows.vue').default
);
Vue.component(
    'admin-shows-insert',
    require('./components/inserts/admin-shows.vue').default
);

Vue.component(
	'admin-dash',
	require('./components/contents/admin-dashboard.vue').default
);

Vue.component(
    'front-movies',
    require('./components/fronts/front-movies.vue').default
);
Vue.component(
    'front-shows',
    require('./components/fronts/front-shows.vue').default
);

Vue.component(
    'master-search',
    require('./components/fronts/master-search.vue').default
);

const app = new Vue({
    el: '#app',
    data() {
            return {
                searchInput: '',
                frontSetsVisible: true,
                searchSetVisible: true,
            };
        },
    methods: {
        prepareComponent() {
        },
        SearchInputChanged(e) {
            if(this.searchInput.length >= 2){
                this.frontSetsVisible = false;
                Event.$emit('searchRequested', this.searchInput)
            }
            else{
                this.frontSetsVisible = true;
                Event.$emit('searchAborted')
            }
            //axios.get('/api/movies').then(response => {this.movies = response.data.data;});
        },
    },
});
