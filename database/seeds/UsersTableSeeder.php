<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Hadis',
            'email' => 'hadis@durmis.com',
            'password' => bcrypt('secretPassword'),
            'role_id' => 3
        ]);
        DB::table('users')->insert([
            'name' => 'Test',
            'email' => 'test@durmis.com',
            'password' => bcrypt('secretPassword'),
            'role_id' => 3
        ]);
        DB::table('users')->insert([
            'name' => 'Mistral',
            'email' => 'test@mistral.com',
            'password' => bcrypt('secretPassword'),
            'role_id' => 3
        ]);
    }
}
