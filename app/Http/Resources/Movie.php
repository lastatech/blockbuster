<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Rating as Rating;
class Movie extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $rating = Rating::where('movie_id', $this->id);
        return [
        'id' => $this->id,
        'title' => $this->title,
        'description' => $this->description,
        'rating' => $rating,
        'cover' => $this->cover,
        'releaseDate' => $this->releaseDate
    ];
    }
}
