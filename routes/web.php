<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->middleware(['admin'])->group(function () {
	Route::get('/dashboard', 'AdminDashboardController@index');
	Route::resources([
	    'users' => 'UserController',
	]);
	Route::resources([
	    'movies' => 'MovieController',
	]);
	Route::resources([
	    'shows' => 'ShowController',
	]);

});