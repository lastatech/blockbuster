<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/users','Api\UserController@index');
Route::middleware('auth:api')->post('/users','Api\UserController@store');
Route::get('/movies','Api\MovieController@index');
Route::middleware('auth:api')->post('/movies','Api\MovieController@store');
Route::get('/search','Api\SearchController@index');
Route::middleware('auth:api')->post('/shows','Api\ShowController@store');
Route::get('/shows','Api\ShowController@index');
Route::middleware('auth:api')->get('/roles','Api\RoleController@index');